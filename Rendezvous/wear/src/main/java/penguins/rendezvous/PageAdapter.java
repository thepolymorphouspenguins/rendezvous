package penguins.rendezvous;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;


public class PageAdapter extends FragmentStatePagerAdapter
{
    private List<Fragment> fraglist;

    public PageAdapter(FragmentManager fragmanager, List<Fragment> fraglist)
    {
        super(fragmanager);
        this.fraglist = fraglist;
    }

    @Override
    public Fragment getItem(int position)
    {
        return this.fraglist.get(position);
    }

    @Override
    public int getCount()
    {
        return this.fraglist.size();
    }
}

