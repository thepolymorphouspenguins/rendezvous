package penguins.rendezvous;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;

import java.util.ArrayList;
import java.util.List;

import penguins.common.Constants;
import penguins.common.MessageClient;


public class PagerActivity extends FragmentActivity
    implements ViewPager.OnPageChangeListener
{
    private Handler        handler;
    private MessageClient  messageClient;
    private PageAdapter    pageAdapter;
    private List<Fragment> pages;
    private String[] initialTopics;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);

        initialTopics = getIntent().getStringArrayExtra(Constants.KEY_TOPIC);

        handler = new Handler();
        messageClient = new MessageClient(this, new MessageApi.MessageListener()
        {
            @Override
            public void onMessageReceived(final MessageEvent messageEvent)
            {
                if (messageEvent.getPath().equals(Constants.MESSAGE_PATH))
                {
                    handler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            pages.add(PageFragment.newInstance(new String(messageEvent.getData())));
                            pageAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });

        pages = getPages();
        pageAdapter = new PageAdapter(getSupportFragmentManager(), pages);
        ViewPager pager = (ViewPager) findViewById(R.id.vpager);
        pager.setAdapter(pageAdapter);
        pager.setOnPageChangeListener(this);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        messageClient.connect();
    }

    @Override
    protected void onStop()
    {
        messageClient.disconnect();
        super.onStop();
    }

    private List<Fragment> getPages()
    {
        List<Fragment> pagelist = new ArrayList<Fragment>();

        if (initialTopics != null)
        {
            for (String s : initialTopics)
            {
                pagelist.add(PageFragment.newInstance(s));
            }
        }

        return pagelist;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d("SCROLL", "Adding page");
        messageClient.sendMessage(null);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}
