package penguins.rendezvous;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import penguins.common.Constants;


public class MainActivity extends Activity
{
    private String[] initialTopics;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialTopics = getIntent().getStringArrayExtra(Constants.KEY_TOPIC);
        Bitmap bitmap = getIntent().getParcelableExtra(Constants.KEY_IMAGE);
        ((Button) findViewById(R.id.launchbutton))
                .setBackground(new BitmapDrawable(getResources(), bitmap));
    }

    public void startPager(View v)
    {
        Intent pagerintent = new Intent(this, PagerActivity.class);
        pagerintent.putExtra(Constants.KEY_TOPIC, initialTopics);
        startActivity(pagerintent);
    }
}