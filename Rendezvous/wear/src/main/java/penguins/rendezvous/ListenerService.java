package penguins.rendezvous;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import penguins.common.Constants;


public class ListenerService extends WearableListenerService
{
    private int notificationId = 0;

    @Override
    public void onDataChanged(DataEventBuffer dataEvents)
    {
        for(DataEvent dataEvent : dataEvents)
        {
            if (dataEvent.getType() == DataEvent.TYPE_CHANGED)
            {
                if (dataEvent.getDataItem().getUri().getPath().equals(Constants.NOTIFICATION_PATH))
                {
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataEvent.getDataItem());
                    String title = dataMapItem.getDataMap().getString(Constants.KEY_TITLE);
                    String text  = dataMapItem.getDataMap().getString(Constants.KEY_TEXT);
                    String[] topics = dataMapItem.getDataMap().getStringArray(Constants.KEY_TOPIC);
                    byte[] data  = dataMapItem.getDataMap().getByteArray(Constants.KEY_DATA);
                    sendNotification(title, text, topics, data);
                }
            }
        }
    }

    private void sendNotification(String title, String text, String[] topics, byte[] data)
    {
        // Intent for starting the main activity.
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.KEY_IMAGE, BitmapFactory.decodeByteArray(data, 0, data.length));
        intent.putExtra(Constants.KEY_TOPIC, topics);
        PendingIntent contentIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Intent for dismissing the notification.
        PendingIntent deleteIntent = PendingIntent.getService(
                this, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(text)
                .setDeleteIntent(deleteIntent)
                .setContentIntent(contentIntent)
                .build();

        NotificationManagerCompat.from(this).notify(notificationId++, notification);
    }
}