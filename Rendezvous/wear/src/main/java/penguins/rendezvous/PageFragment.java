package penguins.rendezvous;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Random;

import penguins.common.Constants;


public class PageFragment extends Fragment
{
    private static int colorint;

    public PageFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.rect_activity_cards, container, false);
    }

    public static PageFragment newInstance(String topic)
    {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_TOPIC, topic);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void	onViewCreated(View view, Bundle savedInstanceState) {
        //CUSTOMIZE THE VIEW HERE (set strings, bgcolors, pass data, etc.)
        Random rand = new Random();
        colorint = rand.nextInt((255 - 0) + 1);
        //View parent = pg.getView();
        TextView ttl = (TextView) view.findViewById(R.id.sg_prefix);
        ttl.setText(getString(R.string.sg_prefix));
        TextView hint = (TextView) view.findViewById(R.id.sg_suggest);
        hint.setText(getArguments().getString(Constants.KEY_TOPIC));
        TextView appname = (TextView) view.findViewById(R.id.app_name);
        appname.setText(getString(R.string.app_name));
        view.setBackgroundColor(Color.rgb(00, colorint, colorint));
    }

    @Override
    public void	onDestroyView() {
        super.onDestroyView();
    }

}
