package penguins.rendezvous;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import penguins.common.Constants;
import penguins.common.MessageClient;


public class MainActivity extends Activity
{
    private static MessageClient messageClient;
    private static Handler       handler;

    // NEW STUFF STARTS HERE
    public static List<PenguinProfile> userList;
    private       Fragment             frag;
    private       FragmentManager      manager;
    private       ListView             list;
    private boolean FSHOWN = false;
    private Prompter prompter;
    // NEW STUFF ENDS HERE


    public static MessageClient getMessageClient()
    {
        return messageClient;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prompter = new Prompter();
        String[] strings = { "beer", "drinking", "hiking", "reading"};
        prompter.addList(0, Arrays.asList(strings));

        handler = new Handler();

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                TwitterList tl = new TwitterList();
                List<String> strs = tl.getTweets("plentyoffish");
                prompter.addList(1, strs);
            }
        }).start();

        messageClient = new MessageClient(this, new MessageApi.MessageListener()
        {
            @Override
            public void onMessageReceived(final MessageEvent messageEvent)
            {
                if (messageEvent.getPath().equals(Constants.MESSAGE_PATH))
                {
                    handler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            // ******************************
                            // TODO: Generate new topic here!
                            // ******************************

                            messageClient.sendMessage(prompter.getTopic().getBytes());
                        }
                    });
                }
            }
        });

        // NEW STUFF STARTS HERE
        ReadJsonData reader = new ReadJsonData();

        byte[] JSON_BYTES = reader.loadJSONFromAsset("users.json", getBaseContext()).getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(JSON_BYTES);

        try
        {
            userList = reader.readJsonStream(stream);
        }
        catch (IOException e) {}

        manager = getFragmentManager();

        CustomList adapter = new CustomList(this, userList);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id)
            {
                FSHOWN = true;
                FragmentTransaction transaction = manager.beginTransaction();
                frag = ListClickFragment.newInstance(userList.get(position));
                transaction.add(R.id.layout, frag);
                transaction.commit();
            }
        });
        // NEW STUFF ENDS HERE
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        messageClient.connect();
    }

    @Override
    protected void onStop()
    {
        messageClient.disconnect();
        super.onStop();
    }

    // NEW STUFF STARTS HERE
    @Override
    public void onBackPressed() {
        if(FSHOWN) {
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            fragmentTransaction.remove(frag);
            fragmentTransaction.commit();
            FSHOWN = false;
        }
        else {
            super.onBackPressed();
        }
    }
    // NEW STUFF ENDS HERE
}