package penguins.rendezvous;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;


/**
 * This Class was written by Jeff Bayntun
 *
 * This Class creates a fragment for a Location in the ListView
 *
 */
public class ListClickFragment extends Fragment {

    private static final String NAME = "Name";
    private static final String IMAGE = "Image";
    private static final String ID = "Penguin";
    private static final String AGE = "Age";
    private static final String CITY = "City";
    private static final String TWITTER = "Twitter";

    private String mName;
    private Integer mImage;
    private Integer mID;
    private Integer mAge;
    private String mCity;
    private String mTwitter;

    private OnFragmentInteractionListener mListener;

    /**
     * Creates and returns a new Fragment with a Bundle of information
     * Base on the parameter Location object
     */
    // TODO: Rename and change types and number of parameters
    public static ListClickFragment newInstance(PenguinProfile l)
    {
        ListClickFragment fragment = new ListClickFragment();
        Bundle args = new Bundle();
        args.putString(NAME, l.getName());
        args.putInt(IMAGE, l.getImage());
        args.putInt(ID, l.getID());
        args.putInt(AGE, l.getAge());
        args.putString(CITY, l.getCity());
        args.putString(TWITTER, l.getTwitterAccount());
        fragment.setArguments(args);
        return fragment;
    }

    public ListClickFragment()
    {
        // Required empty public constructor
    }

    /*
        Sets fragment variables based on bundle values.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mName = getArguments().getString(NAME);
            mImage = getArguments().getInt(IMAGE);
            mID = getArguments().getInt(ID);
            mCity = getArguments().getString(CITY);
            mAge = getArguments().getInt(AGE);
            mTwitter = getArguments().getString(TWITTER);
        }
    }

    /*
        Creates the whole Fragment view, adding the appropriate values that were
        initially provided from the Location object to the appropriate child Views
        in the Fragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_list_click, container, false);

        TextView t = (TextView)rootView.findViewById(R.id.txt);
        t.setText(mName);

        ImageView i = (ImageView)rootView.findViewById(R.id.img);
        i.setImageResource(mImage);

        TextView c = (TextView)rootView.findViewById(R.id.city);
        c.setText("City: " + mCity);

        TextView a = (TextView)rootView.findViewById(R.id.age);
        a.setText("Age: " + mAge);

        Button b = (Button)rootView.findViewById(R.id.btn);

        b.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PenguinProfile profile = null;

               for (PenguinProfile p : MainActivity.userList)
               {
                   if (p.getID() == mID)
                   {
                       profile = p;
                       //TwitterList tl = new TwitterList();
                       // profile[0].getPrompt().addList(2, tl.getTweets(profile[0].getTwitterAccount()));
                   }
               }

               ByteArrayOutputStream stream = new ByteArrayOutputStream();

               if (profile != null)
               {
                   BitmapFactory.decodeResource(getResources(), profile.getImage())
                                .compress(Bitmap.CompressFormat.PNG, 100, stream);


                   // ***********************************
                   // TODO: Generate initial topics here!
                   // ***********************************
                   String[] topics = {"canucks", "politics", "weather"};

                   MainActivity.getMessageClient().sendNotification(
                           getString(R.string.notification_title),
                           getString(R.string.notification_text),
                           topics,
                           stream.toByteArray());
               }

            }
        });

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    /*public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }*/

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}