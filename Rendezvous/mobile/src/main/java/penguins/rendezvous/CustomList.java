package penguins.rendezvous;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class CustomList extends ArrayAdapter<String>
{
    private final Activity context;
    private List<PenguinProfile> userList;


    /*
        Constructor for CustomList, takes a the Activity context and a list of Locations as a parameter
     */
    public CustomList(Activity context, List l)
    {
        super(context, R.layout.list_single, l);
        this.context = context;
        this.userList = l;
    }

    /*
        Creates an individual element in the ListView.  It populates the child views
        with data from the given position in the locations List.
     */
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_single, null, true);


        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);

        txtTitle.setText(userList.get(position).getName());
        txtTitle.setGravity(16);
        imageView.setImageResource(userList.get(position).getImage());
        imageView.setBackgroundColor(Color.GRAY);


        return rowView;
    }
}