package penguins.rendezvous;

import android.content.Context;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class ReadJsonData
{
    private ArrayList<PenguinProfile> Profiles;

    public List<PenguinProfile> readJsonStream(InputStream in) throws IOException
    {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

        Profiles = new ArrayList<PenguinProfile>();
        try
        {
            readMessagesArray(reader);
            return Profiles;
        }
        finally
        {
            reader.close();
        }
    }

    private void readMessagesArray(JsonReader reader) throws IOException {

        reader.beginArray();
        while (reader.hasNext())
        {
            readMessage(reader);
        }
        reader.endArray();
    }

    private void readMessage(JsonReader reader) throws IOException {

        final String UserName = "userName";
        final String Image = "image";
        final String Interests = "interests";
        final String City = "city";
        final String TwitterAccount = "twitterAccount";
        final String Age = "age";
        String u = null;
        Integer i = null;
        List<String> interests = null;
        Integer age = null; //new added
        String city = null; // new
        String ta = null; // new
        reader.beginObject();

        while (reader.hasNext())
        {
            String name = reader.nextName();
            if (name.equals(UserName))
            {
                u = reader.nextString();
            }
            else if(name.equals(Image))
            {
                i = getImageResource(reader.nextInt());
            }
            else if(name.equals(Interests))
            {
                interests = getInterests(reader);
            }
            else if(name.equals(City))
            {
                city = reader.nextString();
            }
            else if(name.equals(Age))
            {
                age = reader.nextInt();
            }
            else if(name.equals(TwitterAccount))
            {
                ta = reader.nextString();
            }
            else
            {
                reader.skipValue();
            }
        }
        reader.endObject();

        if(u != null && i != null && interests != null && city != null && age != null )
        {
            Profiles.add(new PenguinProfile(u, i, interests, city, age, ta));

        }
    }

    public List<String> getInterests(JsonReader reader) throws IOException
    {
        List<String> data = new ArrayList<String>();
        reader.beginArray();
        while(reader.hasNext())
        {
            data.add(reader.nextString());
        }
        reader.endArray();

        return data;
    }

    public String loadJSONFromAsset(String file, Context context)
    {
        String json = null;
        try
        {
            InputStream is = context.getAssets().open(file);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public Integer getImageResource(Integer i)
    {
        switch(i)
        {
            case 1:
                return R.drawable.image1;
            case 2:
                return R.drawable.image2;
            case 3:
                return R.drawable.image3;
            case 4:
                return R.drawable.image4;
            case 5:
                return R.drawable.image5;
            case 6:
                return R.drawable.image6;
            case 7:
                return R.drawable.image7;
            case 8:
                return R.drawable.image8;
        }
        return -1;
    }
}