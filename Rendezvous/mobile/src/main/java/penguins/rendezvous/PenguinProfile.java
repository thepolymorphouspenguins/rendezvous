package penguins.rendezvous;

import java.util.List;


public class PenguinProfile
{
    private String name;
    private Integer image_resource;
    private List<String> interests;
    private Integer id;
    private String city;
    private String twitterAccount;
    private int age;
    private Prompter prompt;

    static int ID;

    PenguinProfile(String n, Integer i, List<String> interests, String c, int a, String t)
    {
        name = n;
        image_resource = i;
        this.interests = interests;
        city = c;
        age = a;
        twitterAccount = t;
        id = ID++;
        prompt = new Prompter();
        prompt.addList(5, this.interests);

    }

    public String getName()
    {
        return name;
    }

    public Integer getImage()
    {
        return image_resource;
    }

    public List<String> getInterests()
    {
        return interests;
    }

    public Integer getID() { return id; }

    public String getTwitterAccount() { return twitterAccount; }

    public Integer getAge() { return age; }

    public String getCity() { return city; }

    public Prompter getPrompt() {return prompt;}
}