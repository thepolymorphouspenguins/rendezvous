package penguins.rendezvous;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class Prompter
{
    private List<Integer> listTypes;
    private Map<Integer, List<String>> listMap;
    private Random randomGenerator;

    public Prompter()
    {
        listTypes = new ArrayList<Integer>();
        listMap = new HashMap<Integer, List<String>>();
        randomGenerator = new Random();
    }

    public void addList(int listID, List<String> list)
    {
        listTypes.add(listID);
        listMap.put(listID, list);
    }

    public String getTopic()
    {
        return getTopic(randomGenerator.nextInt(listTypes.size()));
    }

    public String getTopic(int listID)
    {
        if (listMap.containsKey(listID))
        {
            List<String> list = listMap.get(listID);
            return list.get(randomGenerator.nextInt(list.size()));
        }

        return null;
    }
}