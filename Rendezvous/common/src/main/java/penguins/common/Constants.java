package penguins.common;


public class Constants
{
    public static final String MESSAGE_PATH      = "/message";
    public static final String NOTIFICATION_PATH = "/notification";
    public static final String KEY_TIME          = "time";
    public static final String KEY_TITLE         = "title";
    public static final String KEY_TEXT          = "text";
    public static final String KEY_DATA          = "data";
    public static final String KEY_IMAGE         = "image";
    public static final String KEY_TOPIC         = "topic";
}