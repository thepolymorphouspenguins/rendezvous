package penguins.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;


public class MessageClient implements GoogleApiClient.ConnectionCallbacks
{
    private GoogleApiClient client;
    private MessageApi.MessageListener listener;
    private String remoteNodeId;

    public MessageClient(Context context, MessageApi.MessageListener listener)
    {
        this.listener = listener;
        client = new GoogleApiClient.Builder(context)
               .addConnectionCallbacks(this).addApi(Wearable.API).build();
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        Wearable.MessageApi.addListener(client, listener);
        Wearable.NodeApi.getConnectedNodes(client).setResultCallback(
            new ResultCallback<NodeApi.GetConnectedNodesResult>()
            {
                @Override
                public void onResult(NodeApi.GetConnectedNodesResult getConnectedNodesResult)
                {
                    if (getConnectedNodesResult.getStatus().isSuccess() &&
                        getConnectedNodesResult.getNodes().size() > 0)
                    {
                        remoteNodeId = getConnectedNodesResult.getNodes().get(0).getId();
                    }
                }
            });
    }

    @Override
    public void onConnectionSuspended(int i)
    {

    }

    public void connect()
    {
        client.connect();
    }

    public void disconnect()
    {
        Wearable.MessageApi.removeListener(client, listener);
        client.disconnect();
    }

    public void sendMessage(byte[] data)
    {
        Wearable.MessageApi.sendMessage(client, remoteNodeId, Constants.MESSAGE_PATH, data);
    }

    public void sendNotification(String title, String text, String[] topics, byte[] data)
    {
        if (client.isConnected())
        {
            PutDataMapRequest request = PutDataMapRequest.create(Constants.NOTIFICATION_PATH);

            // Add time in order to make each notification unique.
            request.getDataMap().putDouble(Constants.KEY_TIME,  System.currentTimeMillis());
            request.getDataMap().putString(Constants.KEY_TITLE, title);
            request.getDataMap().putString(Constants.KEY_TEXT, text);
            request.getDataMap().putStringArray(Constants.KEY_TOPIC, topics);
            request.getDataMap().putByteArray(Constants.KEY_DATA, data);
            Wearable.DataApi.putDataItem(client, request.asPutDataRequest());
        }
        else
        {
            Log.e("MessageClient", "Not connected to Google API!");
        }
    }
}